# 1 Build du projet
````bash
$ : ./build.sh
````

# 2 Lancement du projet

dao peut prendre les valeurs suivantes : mongo, bolt, memory

````bash
$ : ./restserver --dao=mongo
````

# 3 Lancement des tests
````bash
$ : go test -v ./tests/RequestAPI_test.go # L'api doit être lancée en parallèle
$ : go test -v ./tests/DaoMeMory-test.go
````