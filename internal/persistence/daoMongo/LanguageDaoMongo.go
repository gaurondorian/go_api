package daomongo

import (
	"encoding/json"
	"errors"
	"internal/entities"
	"internal/persistence/interfaces"
	"internal/persistence/mongodb"
	"log"
)

type LanguageDaoMongoDB struct {
}

var _ interfaces.LanguageDao = (*LanguageDaoMongoDB)(nil)

var mongoL mongodb.MyMongo

var collectionLanguage = "Languages"

func NewLanguageDaoMongo() LanguageDaoMongoDB {
	mongoL = mongodb.NewMyMongo()
	return LanguageDaoMongoDB{}
}

func (s LanguageDaoMongoDB) FindAll() []entities.Language {

	var languages []entities.Language

	res := mongoL.GetAll(collectionLanguage)

	languages = make([]entities.Language, len(res))

	for index, language := range res {
		var st entities.Language

		json.Unmarshal([]byte(language), &st)

		languages[index] = st
	}

	return languages

}

func (s LanguageDaoMongoDB) Find(code string) (*entities.Language, error) {

	var language entities.Language

	var res string = mongoL.Get(collectionLanguage, "code", code)

	if res == "" {
		return nil, errors.New("Le code n'éxiste pas")
	}

	json.Unmarshal([]byte(res), &language)

	return &language, nil
}

func (s LanguageDaoMongoDB) Exists(code string) bool {

	if mongoL.Get(collectionLanguage, "code", code) != "" {
		return true
	}

	return false
}

func (s LanguageDaoMongoDB) Delete(code string) bool {
	return mongoL.Delete(collectionLanguage, "code", code)
}

func (s LanguageDaoMongoDB) Create(language entities.Language) bool {

	languageStr, err := json.Marshal(language)

	if err != nil {
		log.Fatal("Problème lors de la conversion student to json byte")
		return false
	}

	if !s.Exists(language.Code) {
		return mongoL.CreateLanguage(collectionLanguage, string(languageStr))
	}

	return false

}

func (s LanguageDaoMongoDB) Update(language entities.Language) bool {

	languageStr, err := json.Marshal(language)

	if err != nil {
		log.Fatal("Problème lors de la conversion student to json byte")
		return false
	}

	if s.Exists(language.Code) {
		return mongoL.UpdateLanguage(collectionLanguage, string(languageStr))
	}

	return false
}
