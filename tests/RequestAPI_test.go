package tests

import (
	"bytes"
	"encoding/json"
	"internal/entities"
	"io"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

var prefix string = "http://localhost:8080/apiV1"

func TestFindAllStudents(t *testing.T) {

	var data []entities.Student

	req, err := http.Get(prefix + "/students")

	if err != nil {
		t.Error("Erreur lors du GET")
	}

	body, err := io.ReadAll(req.Body)

	json.Unmarshal(body, &data)

	assert.Greater(t, len(data), 1)

}

//Test Conflict with createStudent
/* func TestOrder(t *testing.T) {

	var data []entities.Student

	var expected []int = []int{1, 2, 3, 4}
	var res []int

	req, err := http.Get(prefix + "/students")

	if err != nil {
		t.Error("Erreur lors du GET")
	}

	body, err := io.ReadAll(req.Body)

	json.Unmarshal(body, &data)

	for _, student := range data {
		res = append(res, student.Id)
	}

	assert.Equal(t, expected, res)

} */

func TestStudentsIdNotExist(t *testing.T) {

	req, err := http.Get(prefix + "/students/10000")

	if err != nil {
		t.Error("Erreur lors du GET")
	}

	assert.Equal(t, 404, req.StatusCode)

}

func TestStudentsIdExist(t *testing.T) {

	req, err := http.Get(prefix + "/students/1")

	if err != nil {
		t.Error("Erreur lors du GET")
	}

	assert.Equal(t, 200, req.StatusCode)

}

func TestCreateStudents(t *testing.T) {

	var student entities.Student = entities.Student{21, "Test first name", "Test last name", 21, "TEST"}

	data, _ := json.Marshal(student)

	req, err := http.Post(prefix+"/students", "application/json", bytes.NewBuffer(data))

	if err != nil {
		t.Error("Erreur lors du GET")
	}

	assert.Equal(t, 201, req.StatusCode)

}

func TestCreateStudentsDuplicationId(t *testing.T) {

	var student entities.Student = entities.Student{3, "Test first name", "Test last name", 21, "TEST"}

	data, _ := json.Marshal(student)

	req, err := http.Post(prefix+"/students", "application/json", bytes.NewBuffer(data))

	if err != nil {
		t.Error("Erreur lors du GET")
	}

	assert.Equal(t, 409, req.StatusCode)

}

func TestDeleteStudent(t *testing.T) {

	req, _ := http.NewRequest("DELETE", prefix+"/students/1", nil)

	client := &http.Client{}

	resp, _ := client.Do(req)

	defer resp.Body.Close()

	assert.Equal(t, 200, resp.StatusCode)

}

func TestDeleteStudentIdNotExist(t *testing.T) {

	req, _ := http.NewRequest("DELETE", prefix+"/students/10000", nil)

	client := &http.Client{}

	resp, _ := client.Do(req)

	defer resp.Body.Close()

	assert.Equal(t, 404, resp.StatusCode)

}

func TestUpdateStudent(t *testing.T) {

	var student entities.Student = entities.Student{3, "Test first name", "Test last name", 21, "TEST"}

	var res entities.Student

	data, _ := json.Marshal(student)

	req, _ := http.NewRequest("PUT", prefix+"/students", bytes.NewBuffer(data))

	client := &http.Client{}

	resp, err := client.Do(req)

	if err != nil {
		t.Error(err)
	}

	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	json.Unmarshal(body, &res)

	assert.Equal(t, student, res)
}

func TestUpdateStudentIdNotExist(t *testing.T) {

	var student entities.Student = entities.Student{1000, "Test first name", "Test last name", 21, "TEST"}

	data, _ := json.Marshal(student)

	req, _ := http.NewRequest("PUT", prefix+"/students", bytes.NewBuffer(data))

	client := &http.Client{}

	resp, _ := client.Do(req)

	defer resp.Body.Close()

	assert.Equal(t, 404, resp.StatusCode)

}
